import re
data = []
emailPattern = ".*@.*[a/-zA-Z0-9].com$"
idPattern = re.compile("\D")
with open("data.txt") as f:
    for idx,line in enumerate(f.readlines()):
        if idx != 0:
            line = line.replace("\n", "")
            x = line.split(",")
            checkEmail = re.fullmatch(emailPattern, x[1])
            if checkEmail:
                checkId = re.search(idPattern, x[2])
                if checkId or x[2] == "":
                    print("warning invalid ID")
                else:
                    if int(x[2]) % 2:
                        p = "The " + x[2] + " of " + x[1] + " is odd number"
                    else:
                        p = "The " + x[2] + " of " + x[1] + " is even number"
                    print(p)
            else:
                print("warning invalid Email")